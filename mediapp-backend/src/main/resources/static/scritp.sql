INSERT INTO user_data(id_user, username, password, enabled)
values (1, 'jairyp90@gmail.com', '$2a$10$ju20i95JTDkRa7Sua63JWOChSBc0MNFtG/6Sps2ahFFqN.HCCUMW.',
        '1');

INSERT INTO "role"  (id_role, name, description)
VALUES (1, 'ADMIN', 'Administrador');
INSERT INTO role (id_role, name, description)
VALUES (2, 'USER', 'Usuario');
INSERT INTO role (id_role, name, description)
VALUES (3, 'DBA', 'Admin de bd');

INSERT INTO menu(id_menu, name, icon, url) VALUES (1, 'Dashboard', 'home', '/pages/dashboard');
INSERT INTO menu(id_menu, name, icon, url) VALUES (2, 'Sign', 'account_box', '/pages/sign');
INSERT INTO menu(id_menu, name, icon, url) VALUES (3, 'Patients', 'accessibility', '/pages/patient');



INSERT INTO user_role (id_user, id_role) VALUES (1, 1);
INSERT INTO user_role (id_user, id_role) VALUES (1, 3);

INSERT INTO menu_role (id_menu, id_role) VALUES (1, 1);
INSERT INTO menu_role (id_menu, id_role) VALUES (2, 1);
INSERT INTO menu_role (id_menu, id_role) VALUES (3, 1);

