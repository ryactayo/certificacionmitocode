package com.mitocode.repo;

import com.mitocode.model.Patient;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

//@Repository
public interface IPatientRepo extends IGenericRepo<Patient, Integer> {

  @Query("FROM Patient p WHERE p.dni like %:keySearch% OR LOWER(p.firstName) like %:keySearch% or LOWER(p.lastName) like %:keySearch%")
  List<Patient> search(@Param("keySearch") String keySearch);
}
