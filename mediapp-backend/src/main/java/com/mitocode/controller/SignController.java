package com.mitocode.controller;

import com.mitocode.dto.MedicDTO;
import com.mitocode.dto.PatientDTO;
import com.mitocode.dto.SignDTO;
import com.mitocode.exception.ModelNotFoundException;
import com.mitocode.model.Medic;
import com.mitocode.model.Patient;
import com.mitocode.model.Sign;
import com.mitocode.service.IPatientService;
import com.mitocode.service.ISignService;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/signs")
@RequiredArgsConstructor
public class SignController {

  private final ISignService singService;

  private final IPatientService patientService;

  @Qualifier("defaultMapper")
  private final ModelMapper mapper;

  @Qualifier("patientMapper")
  private final ModelMapper mapperMedic;

  @GetMapping
  public ResponseEntity<List<SignDTO>> findAll() {
    List<SignDTO> list = singService.findAll().stream().map(this::convertToDto)
      .collect(Collectors.toList());
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<SignDTO> findById(@PathVariable("id") Integer id) throws Exception {
    Sign sign = singService.findById(id);
    return new ResponseEntity<>(this.convertToDto(sign), HttpStatus.OK);
  }

  @GetMapping("/patients/{key}")
  public ResponseEntity<List<PatientDTO>> searchPatient(@PathVariable("key") String key)
    throws Exception {
    List<PatientDTO> lista = patientService.search(key.toLowerCase()).stream()
      .map(this::convertToDto).collect(Collectors.toList());
    return new ResponseEntity<>(lista, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<Object> save(@Valid @RequestBody SignDTO sign) throws Exception {
    Sign registrar = singService.save(convertToEntity(sign));
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
      .buildAndExpand(registrar.getIdSigno()).toUri();
    return ResponseEntity.created(location).build();
  }

  @PutMapping("/{id}")
  public ResponseEntity<Sign> update(@PathVariable("id") Integer id,
    @Valid @RequestBody SignDTO sign) throws Exception {
    sign.setIdSigno(id);
    Sign update = singService.update(convertToEntity(sign), id);
    return new ResponseEntity<>(update, HttpStatus.CREATED);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> delete(@PathVariable("id") Integer id) throws Exception {
    Sign p = singService.findById(id);
    if (p == null) {
      throw new ModelNotFoundException("ID NO ENCONTRADO " + id);
    }
    singService.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @GetMapping("/pageable")
  public ResponseEntity<Page<Sign>> listarPageable(Pageable pageable) throws Exception {
    Page<Sign> pacientes = singService.listPageable(pageable);
    return new ResponseEntity<>(pacientes, HttpStatus.OK);
  }

  private SignDTO convertToDto(Sign obj) {
    return mapper.map(obj, SignDTO.class);
  }

  private Sign convertToEntity(SignDTO dto) {
    return mapper.map(dto, Sign.class);
  }

  private PatientDTO convertToDto(Patient obj) {
    return mapperMedic.map(obj, PatientDTO.class);
  }

  private Patient convertToEntity(PatientDTO dto) {
    return mapperMedic.map(dto, Patient.class);
  }

}
