package com.mitocode.dto;

import com.mitocode.model.Patient;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignDTO {

  private Integer idSigno;

  private LocalDateTime fecha;

  private Double temperatura;

  private Integer pulso;

  private String ritmoRespiratorio;

  private PatientDTO paciente;
}
