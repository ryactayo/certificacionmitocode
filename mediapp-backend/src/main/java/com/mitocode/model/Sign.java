package com.mitocode.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sign")
public class Sign {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer idSigno;

  @ManyToOne
  @JoinColumn(name = "id_paciente", nullable = false, foreignKey = @ForeignKey(name = "FK_signo_paciente"))
  private Patient paciente;

  @Column(name = "fecha")
  private LocalDateTime fecha;

  @Column(name = "temperatura")
  private Double temperatura;

  @Column(name = "pulso")
  private Integer pulso;

  @Column(name = "ritmo_respiratorio")
  private String ritmoRespiratorio;


}
