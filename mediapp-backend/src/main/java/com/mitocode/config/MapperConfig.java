package com.mitocode.config;

import com.mitocode.dto.ConsultDTO;
import com.mitocode.dto.MedicDTO;
import com.mitocode.dto.PatientDTO;
import com.mitocode.dto.SignDTO;
import com.mitocode.model.Consult;
import com.mitocode.model.Medic;
import com.mitocode.model.Patient;
import com.mitocode.model.Sign;
import java.time.LocalDateTime;
import org.hibernate.collection.spi.PersistentCollection;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean("defaultMapper")
    public ModelMapper modelMapper(){
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(context -> !(context.getSource() instanceof PersistentCollection));
        return mapper;
    }

    @Bean("medicMapper")
    public ModelMapper medicMapper(){
        ModelMapper mapper = new ModelMapper();
        TypeMap<MedicDTO, Medic> typeMap1 = mapper.createTypeMap(MedicDTO.class, Medic.class);
        typeMap1.addMapping(MedicDTO::getPrimaryName, (dest, v) -> dest.setFirstName((String) v));
        typeMap1.addMapping(MedicDTO::getSurname, (dest, v) -> dest.setLastName((String) v));
        typeMap1.addMapping(MedicDTO::getPhoto, (dest, v) -> dest.setPhotoUrl((String) v));

        TypeMap<Medic, MedicDTO> typeMap2 = mapper.createTypeMap(Medic.class, MedicDTO.class);
        typeMap2.addMapping(Medic::getFirstName, (dest, v) -> dest.setPrimaryName((String) v));
        typeMap2.addMapping(Medic::getLastName, (dest, v) -> dest.setSurname((String) v));
        typeMap2.addMapping(Medic::getPhotoUrl, (dest, v) -> dest.setPhoto((String) v));
        return mapper;
    }

    @Bean("consultMapper")
    public ModelMapper consultMapper(){
        ModelMapper mapper = new ModelMapper();
        TypeMap<Consult, ConsultDTO> typeMap1 = mapper.createTypeMap(Consult.class, ConsultDTO.class);
        typeMap1.addMapping(e-> e.getMedic().getFirstName(), (dest, v) -> dest.getMedic().setPrimaryName((String) v));
        typeMap1.addMapping(e-> e.getMedic().getLastName(), (dest, v) -> dest.getMedic().setSurname((String) v));
        typeMap1.addMapping(e-> e.getMedic().getPhotoUrl(), (dest, v) -> dest.getMedic().setPhoto((String) v));

        //mapper.getConfiguration().setPropertyCondition(context -> !(context.getSource() instanceof PersistentCollection));

        return mapper;
    }

  @Bean("signMapper")
  public ModelMapper signMapper(){
    ModelMapper mapper = new ModelMapper();
    TypeMap<SignDTO, Sign> typeMap1 = mapper.createTypeMap(SignDTO.class, Sign.class);
    typeMap1.addMapping(SignDTO::getFecha, (dest, v) -> dest.setFecha((LocalDateTime) v));
    typeMap1.addMapping(SignDTO::getTemperatura, (dest, v) -> dest.setTemperatura((Double) v));
    typeMap1.addMapping(SignDTO::getPulso, (dest, v) -> dest.setPulso((Integer) v));
    typeMap1.addMapping(SignDTO::getRitmoRespiratorio, (dest,v) -> dest.setRitmoRespiratorio((String) v));

    TypeMap<Sign, SignDTO> typeMap2 = mapper.createTypeMap(Sign.class, SignDTO.class);
    typeMap2.addMapping(Sign::getFecha, (dest, v) -> dest.setFecha((LocalDateTime) v));
    typeMap2.addMapping(Sign::getTemperatura, (dest, v) -> dest.setTemperatura((Double) v));
    typeMap2.addMapping(Sign::getPulso, (dest, v) -> dest.setPulso((Integer) v));
    typeMap2.addMapping(Sign::getRitmoRespiratorio, (dest, v) -> dest.setRitmoRespiratorio((String) v));
    return mapper;
  }

  @Bean("patientMapper")
  public ModelMapper patientMapper(){
    ModelMapper mapper = new ModelMapper();
    TypeMap<PatientDTO, Patient> typeMap1 = mapper.createTypeMap(PatientDTO.class, Patient.class);
    typeMap1.addMapping(PatientDTO::getFirstName, (dest, v) -> dest.setFirstName((String) v));
    typeMap1.addMapping(PatientDTO::getLastName, (dest, v) -> dest.setLastName((String) v));
    typeMap1.addMapping(PatientDTO::getDni, (dest, v) -> dest.setDni((String) v));
    typeMap1.addMapping(PatientDTO::getAddress, (dest,v) -> dest.setAddress((String) v));
    typeMap1.addMapping(PatientDTO::getPhone, (dest,v) -> dest.setPhone((String) v));
    typeMap1.addMapping(PatientDTO::getEmail, (dest,v) -> dest.setEmail((String) v));
    typeMap1.addMapping(PatientDTO::getAddress, (dest,v) -> dest.setAddress((String) v));

    TypeMap<Patient, PatientDTO> typeMap2 = mapper.createTypeMap(Patient.class, PatientDTO.class);
    typeMap2.addMapping(Patient::getFirstName, (dest, v) -> dest.setFirstName((String) v));
    typeMap2.addMapping(Patient::getLastName, (dest, v) -> dest.setLastName((String) v));
    typeMap2.addMapping(Patient::getDni, (dest, v) -> dest.setDni((String) v));
    typeMap2.addMapping(Patient::getAddress, (dest, v) -> dest.setAddress((String) v));
    typeMap2.addMapping(Patient::getPhone, (dest,v) -> dest.setPhone((String) v));
    typeMap2.addMapping(Patient::getEmail, (dest,v) -> dest.setEmail((String) v));
    typeMap2.addMapping(Patient::getAddress, (dest,v) -> dest.setAddress((String) v));
    return mapper;
  }
}
