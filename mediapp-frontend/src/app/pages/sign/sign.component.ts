import { Component, OnInit, ViewChild } from '@angular/core';
import { Sign } from './../../model/sign';
import { SignService } from 'src/app/service/sign.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css'],
})
export class SignComponent implements OnInit {
  isLoadingResults = true;
  cantidad = 0;
  displayedColumns: string[] = ['paciente', 'temperatura', 'fecha', 'acciones'];
  dataSource: MatTableDataSource<Sign>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private signService: SignService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.signService.signoCambio.subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.signService.mensajeCambio.subscribe((data) => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
      });
    });

    this.signService.listarPageable(0, 10).subscribe((data) => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    });
  }

  filtrar(valor: any) {
    this.dataSource.filter = valor.target.value.trim();
  }

  eliminar(idSigno: number) {
    this.signService
      .delete(idSigno)
      .pipe(
        switchMap(() => {
          return this.signService.findAll();
        })
      )
      .subscribe((data) => {
        this.signService.signoCambio.next(data);
        this.signService.mensajeCambio.next('SE ELIMINO');
      });
  }

  mostrarMas(e: any) {
    this.signService
      .listarPageable(e.pageIndex, e.pageSize)
      .pipe(tap(() => (this.isLoadingResults = false)))
      .subscribe((data) => {
        this.cantidad = data.totalElements;
        this.dataSource = new MatTableDataSource(data.content);
        this.dataSource.sort = this.sort;
        this.isLoadingResults = true;
      });
  }
}
