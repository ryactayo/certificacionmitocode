import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Patient } from 'src/app/model/patient';
import { SignService } from 'src/app/service/sign.service';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sign-edition',
  templateUrl: './sign-edition.component.html',
  styleUrls: ['./sign-edition.component.css']
})
export class SignEditionComponent implements OnInit {

  pacientesFiltrados: any;
  errorMsg: string;

  signoForm: FormGroup;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  id: number;
  edicion: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private signService: SignService
  ) { }

  ngOnInit(): void {

    this.signoForm = this.formBuilder.group({
      paciente: new FormControl('', [Validators.required]),
      fecha: [new Date(), Validators.required],
      temperatura: [null, Validators.required],
      pulso: [null, Validators.required],
      ritmoRespiratorio: [null, Validators.required]
    });

    this.signoForm.get('paciente').valueChanges.pipe(tap(() => this.isLoadingResults = true))
      .subscribe(val => {
        if (val && typeof val !== 'object' && val.length > 1) {
          this.signService.buscarPaciente(val).subscribe(results => {
            this.pacientesFiltrados = results;
            this.isLoadingResults = false;
          });
        } else {
          this.isLoadingResults = false;
        }
      });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

  }

  initForm() {
    if (this.edicion) {
      this.signService.findById(this.id).subscribe(data => {
        this.signoForm = this.formBuilder.group({
          idSigno: [data.idSign],
          paciente: new FormControl(data.paciente),
          fecha: [data.fecha],
          temperatura: [data.temperatura],
          pulso: [data.pulso],
          ritmoRespiratorio: [data.ritmoRespiratorio]
        });
      });
    }
  }

  mostrarPaciente(val: Patient) {
    return val ? `${val.firstName} ${val.lastName}` : val;
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    if (this.edicion) {
      this.signService.update(this.signoForm.value,this.id).pipe(switchMap(() => this.signService.findAll()))
        .subscribe((data) => {
          this.signService.signoCambio.next(data);
          this.signService.mensajeCambio.next('SE MODIFICO');
          this.isLoadingResults = false;
        });
    } else {
      this.signService.save(this.signoForm.value).pipe(switchMap(() => this.signService.findAll()))
        .subscribe((data) => {
          this.signService.signoCambio.next(data);
          this.signService.mensajeCambio.next('SE REGISTRO');
          this.isLoadingResults = false;
        });
    }
    this.router.navigate(['pages/sign']);
  }

}