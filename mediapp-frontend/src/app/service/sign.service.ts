import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Sign } from './../model/sign';
import { GenericService } from './generic.service';
import { catchError, retry } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SignService extends GenericService<Sign> {

  signoCambio = new Subject<Sign[]>();
  mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(http, `${environment.HOST}/signs`);
  }

  listarPageable(p: number, s: number) {
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  buscarPaciente(key: string) {
    return this.http.get(`${this.url}/patients/${key}`);
  }

}