import { Patient } from './patient';

export class Sign {
  idSign: number;
  paciente: Patient;
  fecha: Date;
  temperatura: number;
  pulso: number;
  ritmoRespiratorio: string;
}